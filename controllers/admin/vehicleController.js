const { Vehicle } = require('../../models')
const imagekit = require('../../lib/imageKit')
const { Op } = require("sequelize")
const axios = require("axios")

const homepage = async (req, res) => {
    res.render('index')
}

const vehiclesPage = async (req, res) => {
    res.render('vehicles')
}

const detailPage = async (req, res) => {
    const id = req.params.id
    const name = req.params.name

    const query = req.query.q
    console.log(query)
    const item = awaitVehicle.findOne({
        where: {
            [Op.or]: [
                { id: id },
                { name: query }
            ]
        }
    })

    res.render('detail', {
        id,
        name,
        item,
        query
    })
}

const dataPage = async (req, res) => {
    const nm = req.query.name
    const items = await Vehicle.findAll()
    res.render('vehicles', {
        items,
        nm
    })
}

const createPage = async (req, res) => {
    res.render('add')
}

// controller create
const createVehicle = async (req, res) => {
    const { name, price, size, image } = req.body
    const split = req.file.originalname.split('.')
    const ext = split[split.length - 1]

    // upload file to imagekit
    const img = await imagekit.upload({
        file: req.file.buffer,
        fileName: `${req.file.originalname}.${ext}`,
    })
    console.log(img.url)

    const newVehicle = await Vehicle.create({
        name,
        price,
        size,
        image: img.url,
    })
    res.redirect('/admin/vehicles')
}

const editPage = async (req, res) => {
    const item = await Vehicle.findByPk(req.params.id)
    res.render('edit', {
        item
    })
}

// controller edit
const editVehicle = async (req, res) => {
    const { name, price, capacity } = req.body
    const id = req.params.id
    await Vehicle.update({
        name,
        price,
        capacity,
    }, {
        where: {
            id
        }
    })
    res.redirect('/admin/vehicles')
}

// controller delete
const deleteVehicle = async (req, res) => {
    const id = req.params.id
    await Vehicle.destroy({
        where: {
            id
        }
    })
    res.redirect('/admin/vehicles')
}

const findVehicle = async (req, res) => {
    if (req.query.name && req.query.size == "") {
        const nm = req.query.name
        const items = await Vehicle.findAll({
            where: {
                name: { [Op.iLike]: req.query.name ? `%${req.query.name}%` : null }
            }
        })
        res.render('vehicles', {
            items,
            nm
        })
    } else if (req.query.name == "" && req.query.size) {
        const nm = req.query.name
        const items = await Vehicle.findAll({
            where: {
                size: req.query.size ? req.query.size : null
            }
        })
        res.render('vehicles', {
            items,
            nm
        })
    } else if (req.query.name && req.query.size) {
        const nm = req.query.name
        const items = await Vehicle.findAll({
            where: {
                [Op.and]: [
                    { name: { [Op.iLike]: req.query.name ? `%${req.query.name}%` : null } },
                    { size: req.query.size ? req.query.size : null }
                ]
            }
        })
        res.render('vehicles', {
            items,
            nm
        })
    } else {
        const nm = ""
        const items = await Vehicle.findAll()
        res.render('vehicles', {
            items,
            nm
        })
    }
}

module.exports = {
    homepage,
    dataPage,
    createPage,
    createVehicle,
    editPage,
    editVehicle,
    deleteVehicle,
    detailPage,
    vehiclesPage,
    findVehicle
}