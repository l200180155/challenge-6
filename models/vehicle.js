'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Vehicle extends Model {
        static associate(models) {
        }
    }
    Vehicle.init({
        name: DataTypes.STRING,
        price: DataTypes.INTEGER,
        size: DataTypes.STRING,
        image: DataTypes.STRING
    }, {
        sequelize,
        modelName: 'Vehicle',
    });
    return Vehicle;
};